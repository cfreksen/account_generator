import os
import base64
import sys


def generate_user_and_pass(username_length=8, password_length=16):
    for _ in range(1000):
        user_raw = os.urandom((4 + username_length * 5) // 8)
        username_candidate = str(base64.b32encode(user_raw), 'utf-8')
        if not username_candidate[0].isdigit():
            break
    else:
        print('Error: Could not generate proper username after 1000 tries')
        return None, None
    username = username_candidate.lower()[:username_length]

    password_raw = os.urandom((3 + password_length * 3) // 4)
    password = str(base64.urlsafe_b64encode(password_raw), 'utf-8')[:password_length]

    return username, password


if __name__ == '__main__':
    username, password = generate_user_and_pass()
    if username is not None and password is not None:
        print(username)
        print(password)
    else:
        sys.exit(1)


# accgen --- A small script to generate account credentials
# Copyright (C) 2018  Casper Freksen (casper@freksen.dk)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
